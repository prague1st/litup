from article_class import Article
import requests
from bs4 import BeautifulSoup

nature_base = 'http://www.nature.com'
nature_current = '/nature/current_issue.html'

def nature_crawling(keywords):
	paper = Article()
	source_code = requests.get(nature_base+nature_current)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	for division0 in soup.findAll('div',{'class':'inner'}):
		# 'REVIEW' section in Nature
		for division_review in division0.findAll('div',{'id':'rv'}):
			for division_tit in division_review.findAll('div',{'class':'standard-teaser'}):
				Article.count_scanned += 1
				try:
					title = division_tit.a.text
					title = title.lower()
					title = title.replace('-',' ')
					title = title.replace('\n','')

					flag = False
					for item in keywords:
						if title.find(item.lower()) != -1:
							flag = True
							break
						else:
							continue
					if flag == True:
						Article.count_returned += 1
						paper.author = []
						paper.title = division_tit.a.text.replace('\n','')
						# The author set is the 'ul' attribute of title!
						author_set = division_tit.ul
						for items in author_set.findAll('li'):
							paper.author.append(items.text)
							paper.url = nature_base+division_tit.a.get("href")

						with open(Article.filename,'a') as outfile:
							outfile.write('{0}. in Nature (Reviews):\n'.format(Article.count_returned))
						paper.print_science_article_to_file()
						print '{0}. in Nature:'.format(Article.count_returned)
						paper.print_science_article()

				except:
					print "Paper format not standard!!"
					pass

		# 'ARTICLES' section in Nature
		for division_review in division0.findAll('div',{'id':'af'}):
			for division_tit in division_review.findAll('div',{'class':'standard-teaser'}):
				Article.count_scanned += 1
				try:
					title = division_tit.a.text
					title = title.lower()
					title = title.replace('-',' ')
					title = title.replace('\n','')

					flag = False
					for item in keywords:
						if title.find(item.lower()) != -1:
							flag = True
							break
						else:
							continue
					if flag == True:
						Article.count_returned += 1
						paper.author = []
						paper.title = division_tit.a.text.replace('\n','')
						# The author set is the 'ul' attribute of title!
						author_set = division_tit.ul
						for items in author_set.findAll('li'):
							paper.author.append(items.text)
							paper.url = nature_base+division_tit.a.get("href")

						with open(Article.filename,'a') as outfile:
							outfile.write('{0}. in Nature (Articles):\n'.format(Article.count_returned))
						paper.print_science_article_to_file()
						print '{0}. in Nature:'.format(Article.count_returned)
						paper.print_science_article()

				except:
					print "Paper format not standard!!"
					pass

		# 'LETTERS' section in Nature
		for division_review in division0.findAll('div',{'id':'lt'}):
			for division_tit in division_review.findAll('div',{'class':'standard-teaser'}):
				Article.count_scanned += 1
				try:
					title = division_tit.a.text
					title = title.lower()
					title = title.replace('-',' ')
					title = title.replace('\n','')

					flag = False
					for item in keywords:
						if title.find(item.lower()) != -1:
							flag = True
							break
						else:
							continue
					if flag == True:
						Article.count_returned += 1
						paper.author = []
						paper.title = division_tit.a.text.replace('\n','')
						# The author set is the 'ul' attribute of title!
						author_set = division_tit.ul
						for items in author_set.findAll('li'):
							paper.author.append(items.text)
							paper.url = nature_base+division_tit.a.get("href")

						with open(Article.filename,'a') as outfile:
							outfile.write('{0}. in Nature (Letters):\n'.format(Article.count_returned))
						paper.print_science_article_to_file()
						print '{0}. in Nature:'.format(Article.count_returned)
						paper.print_science_article()

				except:
					print "Paper format not standard!!"
					pass


def nature_physics_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/nphys/current_issue/')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'News and Views' section in Nature Physics
	for division0 in soup.findAll('div',{'id':'nv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Physics (News and Views):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Physics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Review' section in Nature Physics
	for division0 in soup.findAll('div',{'id':'rv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Physics (Review):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Physics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Progress Article' section in Nature Physics
	for division0 in soup.findAll('div',{'id':'prog'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Physics (Progress Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Physics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Letter' section in Nature Physics
	for division0 in soup.findAll('div',{'id':'le'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Physics (Letter):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Physics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Article' section in Nature Physics
	for division0 in soup.findAll('div',{'id':'af'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Physics (Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Physics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def nature_materials_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/nmat/current_issue/')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'News and Views' section in Nature Materials
	for division0 in soup.findAll('div',{'id':'nv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Materials (News and Views):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Review' section in Nature Materials
	for division0 in soup.findAll('div',{'id':'rv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Materials (Review):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Progress Article' section in Nature Materials
	for division0 in soup.findAll('div',{'id':'prog'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Materials (Progress Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Letter' section in Nature Materials
	for division0 in soup.findAll('div',{'id':'le'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Materials (Letter):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Article' section in Nature Materials
	for division0 in soup.findAll('div',{'id':'af'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Materials (Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

def nature_nano_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/nnano/current_issue/')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'News and Views' section in Nature Nanotechnology
	for division0 in soup.findAll('div',{'id':'nv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Nanotechnology (News and Views):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Nanotechnology:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Review' section in Nature Nanotechnology
	for division0 in soup.findAll('div',{'id':'rv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Nanotechnology (Review):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Nanotechnology:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Progress Article' section in Nature Nanotechnology
	for division0 in soup.findAll('div',{'id':'prog'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Nanotechnology (Progress Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Nanotechnology:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Letter' section in Nature Nanotechnology
	for division0 in soup.findAll('div',{'id':'le'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Nanotechnology (Letter):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Nanotechnology:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Article' section in Nature Nanotechnology
	for division0 in soup.findAll('div',{'id':'af'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Nanotechnology (Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Nanotechnology:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

def nature_photon_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/nphoton/current_issue/')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'News and Views' section in Nature Photonics
	for division0 in soup.findAll('div',{'id':'nv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Photonics (News and Views):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Photonics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Review' section in Nature Photonics
	for division0 in soup.findAll('div',{'id':'rv'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Photonics (Review):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Photonics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Progress Article' section in Nature Photonics
	for division0 in soup.findAll('div',{'id':'prog'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Photonics (Progress Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Photonics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Letter' section in Nature Photonics
	for division0 in soup.findAll('div',{'id':'le'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Photonics (Letter):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Photonics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Article' section in Nature Photonics
	for division0 in soup.findAll('div',{'id':'af'}):
		for division_tit in division0.findAll('h4',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Photonics (Article):\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Photonics:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def nature_comm_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/ncomms/archive/subject/npg_subject_639/index.html')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	for division_tit in soup.findAll('h3',{'class':'title'}):
		Article.count_scanned += 1
		print
		try:
			title = division_tit.text
			title = title.lower()
			title = title.replace('-',' ')
			title = title.replace('\n','')

			flag = False
			for item in keywords:
				if title.find(item.lower()) != -1:
					flag = True
					break
				else:
					continue
			if flag == True:
				Article.count_returned += 1
				paper.title = division_tit.text.replace('\n','')
				paper.author = division_tit.parent.parent.next_sibling.next_sibling.next_sibling.\
							next_sibling.text.replace('\t','')
				paper.author = paper.author.replace('\n','')
				paper.url = division_tit.a.get("href").replace('\n','')

				with open(Article.filename,'a') as outfile:
					outfile.write('{0}. in Nature Communications:\n'.format(Article.count_returned))
				paper.print_article_to_file()
				print '{0}. in Nature Communications:'.format(Article.count_returned)
				paper.print_article()

		except:
			print "Paper format not standard!!"
			pass
			

def nature_lsa_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/lsa/current_issue/')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	for division0 in soup.findAll('div',{'class':'container'}):
		for division_tit in division0.findAll('h5',{'class':'atl'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.next_sibling.text.replace("\n"," ")
					paper.author = paper.author.replace("\t","")
					paper.url = nature_base+division_tit.next_sibling.next_sibling.next_sibling.next_sibling.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Light: Science and Applications:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Nature Light: Science and Applications:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def nature_scirep_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://www.nature.com/srep/articles')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	for division0 in soup.findAll('div',{'class':'cleared'}):
		for division_tit in division0.findAll('h3',{'itemprop':'name headline'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.author = []
					paper.title = division_tit.text.replace('\n','')
					author_set = division_tit.next_sibling
					for items in author_set.findAll('span',{'itemprop':'name'}):
						paper.author.append(items.text)
					paper.url = nature_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nature Scientific Reports:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in Nature Scientific Reports:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass
