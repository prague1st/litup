# README #

##========= This package is for: =========

Quickly fetching the newest updates from mainstream journals in the field of physics/optoelectronics/materials.

##========= What you need to do: =========

Provide key words relevant to your area of interest in the main file ('Literature_update.py'), and run!

This program scans a basket of journals including: Nature series, Science series, Physical Review series (PRL, A, B, E, PRApplied, PRX), AIP series (APL and JAP), ACS series (JACS, ACSPhotonics), and Advanced Materials series.


##========= Setting up: =========

This package is written and built under Python 2.7.6. To run it, just type in terminal:

* python Literature_update.py

You will need two additional library installed beforehand. One is 'requests', which fetches an html file given an address. Use

* pip install requests

to install. The other necessary library is beautifulsoup, which parses an html document and filters objects from that file. A comprehensive tutorial including installation details can be found at: 

### https://www.crummy.com/software/BeautifulSoup/bs4/doc/


##Questions?
Please contact the author at: 

* yufei@psu.edu