from article_class import Article
import requests
from bs4 import BeautifulSoup

science_base = "http://www.sciencemag.org"
science_current = '/content/current'
scienceadv_base = "http://advances.sciencemag.org/"

def science_crawling(keywords):
	paper = Article()
	source_code = requests.get(science_base+science_current)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'Research Article' section in science
	for division0 in soup.findAll('div',{'class':'toc-level level2 pub-section-ResearchArticles no-children has-parent'}):
		for division_tit in division0.findAll('h4',{'class':'cit-first-element cit-title-group'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.author = []
					paper.title = division_tit.text.replace('\n','')
					# The author set is two siblings next to the title!
					author_set = division_tit.next_sibling.next_sibling
					for items in author_set.findAll('span',{'class':'cit-auth-type-author'}):
						paper.author.append(items.text)

					url_set = division_tit.parent.next_sibling.next_sibling.next_sibling.next_sibling
					for items in url_set.findAll('li',{'class':'first-item'}):
						paper.url = science_base+items.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Science:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in Science:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Review' section in Science
	for division0 in soup.findAll('div',{'class':'toc-level level2 pub-section-Reviews no-children has-parent'}):
		for division_tit in division0.findAll('h4',{'class':'cit-first-element cit-title-group'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.author = []
					paper.title = division_tit.text.replace('\n','')
					# The author set is two siblings next to the title!
					author_set = division_tit.next_sibling.next_sibling
					for items in author_set.findAll('span',{'class':'cit-auth-type-author'}):
						paper.author.append(items.text)

					url_set = division_tit.parent.next_sibling.next_sibling
					for items in url_set.findAll('li',{'class':'first-item'}):
						paper.url = science_base+items.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Science:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in Science:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Reports' section in Science
	for division0 in soup.findAll('div',{'class':'toc-level level2 pub-section-Reports no-children has-parent'}):
		for division_tit in division0.findAll('h4',{'class':'cit-first-element cit-title-group'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.author = []
					paper.title = division_tit.text.replace('\n','')
					# The author set is two siblings next to the title!
					author_set = division_tit.next_sibling.next_sibling
					for items in author_set.findAll('span',{'class':'cit-auth-type-author'}):
						paper.author.append(items.text)

					url_set = division_tit.parent.next_sibling.next_sibling.next_sibling.next_sibling
					for items in url_set.findAll('li',{'class':'first-item'}):
						paper.url = science_base+items.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Science:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in Science:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass

def scienceadv_crawling(keywords):
	paper = Article()
	source_code = requests.get(scienceadv_base)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'Latest online' section in science advances
	for division_tit in soup.findAll('div',{'class':'highwire-cite-title media__headline__title'}):
		try:
			Article.count_scanned += 1
			title = division_tit.text
			title = title.lower()
			title = title.replace('-',' ')
			title = title.replace('\n','')

			flag = False
			for item in keywords:
				if title.find(item.lower()) != -1:
					flag = True
					break
				else:
					continue

			if flag == True:
				Article.count_returned += 1
				paper.author = []
				paper.title = division_tit.text.replace('\n','')
				# The author set is two siblings next to the title!
				author_set = division_tit.parent.parent.next_sibling.next_sibling
				for items in author_set.findAll('span',{'class':'highwire-citation-author'}):
					paper.author.append(items.text)
				url_set = author_set.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling
				for items in url_set.findAll('li',{'class':'abstract first'}):
					paper.url = scienceadv_base+items.a.get('href')

				with open(Article.filename,'a') as outfile:
					outfile.write('{0}. in Science Advances:\n'.format(Article.count_returned))
				paper.print_science_article_to_file()
				print '{0}. in Science Advances:'.format(Article.count_returned)
				paper.print_science_article()

		except:
			print "Paper format not standard!!"
			pass
