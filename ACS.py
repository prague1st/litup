from article_class import Article
import requests
from bs4 import BeautifulSoup

ACS_base = 'http://pubs.acs.org/toc'

def JACS_crawling(keywords):
	paper = Article()
	source_code = requests.get(ACS_base+'/jacsat/current')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'titleAndAuthor'}):
		for division_tit in division0.findAll('a',{'class':'ref nowrap'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.url = ACS_base+division_tit.get('href')
					paper.author = []
					for items in division0.findAll('span',{'class':'entryAuthor normal hlFld-ContribAuthor'}):
						paper.author.append(items.text)

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in JACS:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in JACS:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass


def ACSphoton_crawling(keywords):
	paper = Article()
	source_code = requests.get(ACS_base+'/apchd5/current')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'titleAndAuthor'}):
		for division_tit in division0.findAll('a',{'class':'ref nowrap'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.url = ACS_base+division_tit.get('href')
					paper.author = []
					for items in division0.findAll('span',{'class':'entryAuthor normal hlFld-ContribAuthor'}):
						paper.author.append(items.text)

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in ACS Photonics:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in ACS photonics:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass

def ACSnano_crawling(keywords):
	paper = Article()
	source_code = requests.get(ACS_base+'/ancac3/current')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'titleAndAuthor'}):
		for division_tit in division0.findAll('a',{'class':'ref nowrap'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.url = ACS_base+division_tit.get('href')
					paper.author = []
					for items in division0.findAll('span',{'class':'entryAuthor normal hlFld-ContribAuthor'}):
						paper.author.append(items.text)

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in ACS Nano:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in ACS nano:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass

def NanoLett_crawling(keywords):
	paper = Article()
	source_code = requests.get(ACS_base+'/nalefd/current')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'titleAndAuthor'}):
		for division_tit in division0.findAll('a',{'class':'ref nowrap'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.url = ACS_base+division_tit.get('href')
					paper.author = []
					for items in division0.findAll('span',{'class':'entryAuthor normal hlFld-ContribAuthor'}):
						paper.author.append(items.text)

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Nano Letter:\n'.format(Article.count_returned))
					paper.print_science_article_to_file()
					print '{0}. in Nano Letter:'.format(Article.count_returned)
					paper.print_science_article()

			except:
				print "Paper format not standard!!"
				pass
