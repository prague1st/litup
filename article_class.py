import time
import os

class Article:
	""" Represents an article,

	with title, author names, and url"""

	count_scanned = 0
	count_returned = 0
	filename = time.strftime("%Y%m%d")+'.txt'

	def __int__(self,title,author,url):
		# Initialize the title of article
		self.title = title
		self.author = author
		self.url = url

	def print_article(self):
		print 'TITLE:	'+self.title
		print 'AUTHORS:	'+self.author
		print self.url+'\n'

	def print_science_article(self):
		print 'TITLE:	'+self.title
		print 'AUTHORS: '+', '.join(self.author)
		print self.url+'\n'
	
	def print_article_to_file(self):
		tit = self.title.encode('ascii','ignore')
		aut = self.author.encode('ascii','ignore')
		with open(self.filename,'a') as outfile1:
			outfile1.write('TITLE:		'+tit+'\n')
			outfile1.write('AUTHORS:	'+aut+'\n')
			outfile1.write(self.url+'\n\n')

	def print_science_article_to_file(self):
		tit = self.title.encode('ascii','ignore')
		aut = []
		for items in self.author:
			aut.append(items.encode('ascii','ignore'))
		with open(self.filename,'a') as outfile:
			outfile.write('TITLE:		'+tit+'\n')
			outfile.write('AUTHORS: '+', '.join(aut)+'\n')
			outfile.write(self.url+'\n\n\n')

	@classmethod
	def how_many(cls):
		print 'Total publications scanned are: {0}'.format(cls.count_scanned)
		print 'Total matched results are: {0}'.format(cls.count_returned)
