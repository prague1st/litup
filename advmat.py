from article_class import Article
import requests
from bs4 import BeautifulSoup

wiley_base = 'http://onlinelibrary.wiley.com'

def advmat_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-4095/currentissue')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'Review' section in Advanced Materials
	for division0 in soup.findAll('li',{'id':'group7'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Communications' section in Advanced Materials
	for division0 in soup.findAll('li',{'id':'group8'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# Another 'Communications' section in Advanced Materials
	for division0 in soup.findAll('li',{'id':'group10'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# One more 'Communications' section in Advanced Materials
	for division0 in soup.findAll('li',{'id':'group12'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def advfuncmat_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1616-3028/currentissue')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'Full paper' section in Advanced Functional Materials
	for division0 in soup.findAll('li',{'id':'group7'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Functional Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Functional Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

def advenergymat_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1614-6840/currentissue')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'Review' section in Advanced Energy Materials
	for division0 in soup.findAll('li',{'id':'group7'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Energy Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Energy Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Communications' section in Advanced Energy Materials
	for division0 in soup.findAll('li',{'id':'group8'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Energy Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Energy Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Full paper' section in Advanced Energy Materials
	for division0 in soup.findAll('li',{'id':'group9'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Energy Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Energy Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# Another 'Full paper' section in Advanced Energy Materials
	for division0 in soup.findAll('li',{'id':'group11'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Energy Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Energy Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

def advoptmat_crawling(keywords):
	paper = Article()
	source_code = requests.get('http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2195-1071/currentissue')
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)

	# 'Review' section in Advanced Optical Materials
	for division0 in soup.findAll('li',{'id':'group6'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Optical Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Optical Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Communications' section in Advanced Optical Materials
	for division0 in soup.findAll('li',{'id':'group7'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Optical Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Optical Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# 'Full paper' section in Advanced Optical Materials
	for division0 in soup.findAll('li',{'id':'group9'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Optical Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Optical Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# Another 'Full paper' section in Advanced Optical Materials
	for division0 in soup.findAll('li',{'id':'group11'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Optical Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Optical Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass

	# One more 'Full paper' section in Advanced Optical Materials
	for division0 in soup.findAll('li',{'id':'group13'}):
		for division_tit in division0.findAll('div',{'class':'citation tocArticle'}):
			Article.count_scanned += 1
			try:
				title = division_tit.a.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.a.text.replace('\n','')
					paper.author = division_tit.a.next_sibling.text
					paper.url = wiley_base+division_tit.a.get("href")

					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in Advanced Optical Materials:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in Advanced Optical Materials:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass
