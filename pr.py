from article_class import Article
import requests
from bs4 import BeautifulSoup

pr_base = "http://journals.aps.org"
prl_recent = "/prl/recent"
prb_recent = "/prb/recent"
pre_recent = "/pre/recent"
prapplied_recent = "/prapplied/recent"
prx_recent = "/prx/recent"

def prl_crawling(keywords):
	paper = Article()
	source_code = requests.get(pr_base+prl_recent)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'large-9 columns'}):
		for division_tit in division0.findAll('h5',{'class':'title'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text
					paper.url = pr_base+division_tit.a.get("href")
					
					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in PRL:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in PRL:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def prb_crawling(keywords):
	paper = Article()
	source_code = requests.get(pr_base+prb_recent)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'large-9 columns'}):
		for division_tit in division0.findAll('h5',{'class':'title'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text
					paper.url = pr_base+division_tit.a.get("href")
					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in PRB:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in PRB:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def pre_crawling(keywords):
	paper = Article()
	source_code = requests.get(pr_base+pre_recent)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'large-9 columns'}):
		for division_tit in division0.findAll('h5',{'class':'title'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text
					paper.url = pr_base+division_tit.a.get("href")
					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in PRE:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in PRE:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def prapplied_crawling(keywords):
	paper = Article()
	source_code = requests.get(pr_base+prapplied_recent)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'large-9 columns'}):
		for division_tit in division0.findAll('h5',{'class':'title'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text
					paper.url = pr_base+division_tit.a.get("href")
					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in PRApplied:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print '{0}. in PRApplied:'.format(Article.count_returned)
					paper.print_article()

			except:
				print "Paper format not standard!!"
				pass


def prx_crawling(keywords):
	paper = Article()
	source_code = requests.get(pr_base+prx_recent)
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text)
	for division0 in soup.findAll('div',{'class':'large-9 columns'}):
		for division_tit in division0.findAll('h5',{'class':'title'}):
			Article.count_scanned += 1
			try:
				title = division_tit.text
				title = title.lower()
				title = title.replace('-',' ')
				title = title.replace('\n','')

				flag = False
				for item in keywords:
					if title.find(item.lower()) != -1:
						flag = True
						break
					else:
						continue
				if flag == True:
					Article.count_returned += 1
					paper.title = division_tit.text.replace('\n','')
					paper.author = division_tit.next_sibling.text
					paper.url = pr_base+division_tit.a.get("href")
					with open(Article.filename,'a') as outfile:
						outfile.write('{0}. in PRX:\n'.format(Article.count_returned))
					paper.print_article_to_file()
					print "{0}. in PRX:".format(Article.count_returned)
					paper.print_article()
					
			except:
				print "Paper format not standard!!"
				pass
