# ========================================================================
#	 Programmer			Data		Version
#	 Yufei Shen 	12/20/2015		1.0
# ========================================================================

import os
import time

from article_class import Article
import nature
import pr 	# physical review series of journals
import science
import ACS
import advmat
# import aip	# AIP journals including apl and jap

tic = time.time()

keywords = ['organic','semiconductor','light','OLED','luminescence','concentrat',\
			'photo','film','optoelectric','photonic','plasmon','laser','lasing',\
			'electronic','flexible','LED']

# keywords = ['sensit']
try:
	os.remove(Article.filename)
except:
	pass

pr.prl_crawling(keywords)
pr.prb_crawling(keywords)
pr.pre_crawling(keywords)
pr.prapplied_crawling(keywords)
pr.prx_crawling(keywords)

science.science_crawling(keywords)
science.scienceadv_crawling(keywords)

nature.nature_crawling(keywords)
nature.nature_physics_crawling(keywords)
nature.nature_materials_crawling(keywords)
nature.nature_nano_crawling(keywords)
nature.nature_photon_crawling(keywords)
nature.nature_lsa_crawling(keywords)
nature.nature_scirep_crawling(keywords)
nature.nature_comm_crawling(keywords)

advmat.advmat_crawling(keywords)
advmat.advfuncmat_crawling(keywords)
advmat.advenergymat_crawling(keywords)
advmat.advoptmat_crawling(keywords)

ACS.JACS_crawling(keywords)
ACS.ACSphoton_crawling(keywords)
ACS.ACSnano_crawling(keywords)
ACS.NanoLett_crawling(keywords)

Article.how_many()

toc = time.time()
print('\nSearching takes '+str(toc-tic)+'secs')
